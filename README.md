
<div align="center">

<img src="./app/src/main/ic_launcher-playstore.png" alt="LOGO"/>

# 拖放中转站 Lite

</div>

中转任何拖放事件。用最小的APK，给用户较好的体验。

[![star](https://gitee.com/Jesse205/DragAndDropTransferStationLite/badge/star.svg?theme=dark)](https://gitee.com/Jesse205/DragAndDropTransferStationLite/stargazers)
[![fork](https://gitee.com/Jesse205/DragAndDropTransferStationLite/badge/fork.svg?theme=dark)](https://gitee.com/Jesse205/DragAndDropTransferStationLite/members)

## 安装教程

请参考[《刷机指南》](https://efadg.netlify.app/)

## 注意事项

- 必须给予“在其他应用上层显示”权限；
- 需要 Android 7.0 以上；
- 仅当有拖拽事件发生时才显示悬浮窗；
- 部分应用不支持从外部拖入、启动拖拽、中转拖拽，请以实际情况为准；
- 更多请在 APP 内查看。

## 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

## 开源许可

- **[DocumentsUI-lineage](https://github.com/LineageOS/android_packages_apps_DocumentsUI)**: [Apache-2.0](http://www.apache.org/licenses/LICENSE-2.0)
- **[Android Jetpack](https://github.com/androidx/androidx)**: [Apache-2.0](https://github.com/androidx/androidx/blob/androidx-main/LICENSE.txt)
- **[Chromium](https://github.com/chromium/chromium)**: [BSD 3-Clause "New" or "Revised" License](https://github.com/chromium/chromium/blob/main/LICENSE)
- **[RikkaX](https://github.com/RikkaApps/RikkaX)**: [MIT](https://github.com/RikkaApps/RikkaX/blob/master/LICENSE)

## 交流讨论

- [QQ 群](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=UuYebjPK0r9-tSLU8T09qB3Cuq04rdjQ&authKey=B%2Fw7P3MjRll8i5lQxYRTHO3Yhx17KGMfpmUSSO0EDAy%2Fhi1o4irhk1qZ9MC0j4Ou&noverify=0&group_code=331680482)