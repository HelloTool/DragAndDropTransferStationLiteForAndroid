package io.github.jesse205.transferstation.lite.sources;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class AppSource {
    public static final String STATE = "state";
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private boolean state = false;

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        changeSupport.firePropertyChange(STATE, this.state, state);
        this.state = state;
    }

    public void addListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
}
