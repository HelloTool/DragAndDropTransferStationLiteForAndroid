package io.github.jesse205.transferstation.lite.activities;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import io.github.jesse205.transferstation.lite.DragDropApplication;
import io.github.jesse205.transferstation.lite.R;
import io.github.jesse205.transferstation.lite.databinding.PanelLayoutBinding;
import io.github.jesse205.transferstation.lite.helpers.ThemeHelper;
import io.github.jesse205.transferstation.lite.managers.DragPanelManager;
import io.github.jesse205.transferstation.lite.services.DragDropService;

public class DragDropActivity extends Activity {
    public static final String EXTRA_CALLBACK = "callback";
    public static final String EXTRA_KEEP_IN_SCREEN = "keep_in_screen";
    private static final String TAG = "DragAndDropActivity";
    private static boolean needKeepVisibleLastTime = false;
    private DragDropApplication application;
    @Nullable
    private DragDropService.DragAndDropServiceBinder appService;
    @Nullable
    private BridgeCallback callback;
    @Nullable
    private PanelLayoutBinding binding;
    @Nullable
    private PanelLayoutBinding binding2;
    @Nullable
    private DragPanelManager dragPanelManager;

    private boolean isDropPageEnabled = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (DragDropApplication) getApplication();

        appService = application.getAppService();
        if (appService == null) {
            needKeepVisibleLastTime = true;
            application.setAppEnabled(true);
            application.postWhenAppServiceStarted(this::recreate);
            return;
        }
        needKeepVisibleLastTime = getIntent().getBooleanExtra(EXTRA_KEEP_IN_SCREEN, false);
        isDropPageEnabled = application.isDropPageEnabled();
        if ((!isDropPageEnabled || !appService.getPanelPageState()) && !needKeepVisibleLastTime) {
            moveTaskToBack(true);
        }
        ThemeHelper.applyTheme(this);
        setTheme(R.style.ThemeOverlay_DragAndDropTransferStationLite_NoActionBar_Transparent);
        callback = appService;
        binding = PanelLayoutBinding.inflate(LayoutInflater.from(this));

        DragPanelManager.ViewsHolder[] viewsHolders;
        if (isDropPageEnabled) {
            binding2 = PanelLayoutBinding.inflate(LayoutInflater.from(this));
            View rootView = binding2.getRoot();
            setContentView(rootView);
            rootView.setFitsSystemWindows(true);
            // 使面板填充整个布局
            ViewGroup.LayoutParams layoutParams = binding2.panelView.getLayoutParams();
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            binding2.panelView.setLayoutParams(layoutParams);
            viewsHolders = new DragPanelManager.ViewsHolder[]{
                    new DragPanelManager.ViewsHolder(binding, true),
                    new DragPanelManager.ViewsHolder(binding2, false),
            };
            Window window = getWindow();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                window.setDecorFitsSystemWindows(false);
                window.setNavigationBarContrastEnforced(false);
                window.setStatusBarContrastEnforced(false);
            } else {
                int systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;

                if (ThemeHelper.getThemeBoolean(this, R.attr.isLightTheme)) {
                    systemUiVisibility |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        systemUiVisibility |= View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                    }
                }
                window.getDecorView().setSystemUiVisibility(systemUiVisibility);
            }

        } else {
            viewsHolders = new DragPanelManager.ViewsHolder[]{
                    new DragPanelManager.ViewsHolder(binding, true),
            };
        }

        dragPanelManager = new DragPanelManager(this, viewsHolders,
                appService.getPanelChangeSource());


        dragPanelManager.addPanelToWindow();

        needKeepVisibleLastTime = false;
        callback.onActivityCreate(this, savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (appService != null && !isDropPageEnabled) {
            moveTaskToBack(true);
            return;
        }
        if (!isFinishing()) {
            if (appService != null) {
                appService.setPanelPageState(true);
            }
            if (dragPanelManager != null) {
                dragPanelManager.removePanelFromWindow();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isDropPageEnabled && !isFinishing()) {
            if (appService != null) {
                appService.setPanelPageState(false);
            }
            if (dragPanelManager != null) {
                dragPanelManager.addPanelToWindow();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (callback != null) {
            callback.onActivityDestory(this);
        }
        if (dragPanelManager != null) {
            dragPanelManager.onDestroy();
        }
        Log.d(TAG, "onDestroy: Service activity destroyed");
    }

    @NonNull
    public DragPanelManager getDragPanelManager() {
        assert dragPanelManager != null;
        return dragPanelManager;
    }

    /* @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if (keyCode == KeyEvent.KEYCODE_BACK)
            ((DADTSApplication) getApplication()).setAppEnabled(false);
        return false;
    } */

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public interface BridgeCallback {
        public void onActivityCreate(DragDropActivity activity, @Nullable Bundle savedInstanceState);

        public void onActivityDestory(DragDropActivity activity);
    }
}
