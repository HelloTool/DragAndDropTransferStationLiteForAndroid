package io.github.jesse205.transferstation.lite;

public class DragDropPreferences {
    public static final String KEY_ENABLE_APP = "enable_app";
    public static final String KEY_ADSORB = "adsorb";
    public static final String KEY_POSITION_X = "position_x";
    public static final String KEY_POSITION_Y = "position_y";
    public static final String KEY_SHOW_PANEL = "show_panel";
    public static final String KEY_VERSION = "version";
    public static final String KEY_CLEAR_PANEL_DATA = "clear_panel_data";
    public static final String KEY_ENABLE_DROP_PAGE = "enable_drop_page";
    public static final String KEY_NOTE = "note";
}
