package io.github.jesse205.transferstation.lite.sources;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class PanelSource {
    public static final String STATE = "state";
    public static final String PAGE_STATE = "page_state";
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private boolean state = false;
    private boolean pageState = false;

    public boolean getPageState() {
        return pageState;
    }

    public void setPageState(boolean pageState) {
        changeSupport.firePropertyChange(PAGE_STATE, this.pageState, state);
        this.pageState = pageState;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        changeSupport.firePropertyChange(STATE, this.state, state);
        this.state = state;
    }

    public void addListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
}
