package io.github.jesse205.transferstation.lite.utils;

import android.support.annotation.NonNull;

public class ExceptionUtil {
    @NonNull public static String getStackTraceString(@NonNull Throwable e) {
        StackTraceElement[] traceElements = e.getStackTrace();
        StringBuilder traceBuilder = new StringBuilder();
        for (StackTraceElement traceElement : traceElements) {
            traceBuilder.append(traceElement.toString());
            traceBuilder.append("\n");
        }
        return traceBuilder.toString();
    }

    @NonNull public static String buildErrorMessage(@NonNull Exception e) {
        String stackTrace = getStackTraceString(e);
        String exceptionType = e.toString();
        String exceptionMessage = e.getMessage();
        return String.format("%s : %s \r\n %s", exceptionType, exceptionMessage, stackTrace);
    }

}
