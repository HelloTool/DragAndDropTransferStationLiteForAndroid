package io.github.jesse205.transferstation.lite;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.github.jesse205.transferstation.lite.helpers.ThemeHelper;

public class IconResources {
    public static final int ICON_FILE_MUSIC = R.drawable.file_music_outline;
    public static final int ICON_FILE_VIDEO = R.drawable.file_video_outline;
    public static final int ICON_FILE_IMAGE = R.drawable.file_image_outline;
    public static final int ICON_FILE_TEXT = R.drawable.file_document_outline;
    public static final int ICON_FILE_POWERPOINT = R.drawable.file_powerpoint_outline;
    public static final int ICON_FILE_EXCEL = R.drawable.file_excel_outline;
    public static final int ICON_FILE_WORD = R.drawable.file_word_outline;
    public static final int ICON_FILE_ZIP = R.drawable.zip_box_outline;
    public static final int ICON_FILE_PLUS = R.drawable.file_plus_outline;
    public static final int ICON_FILE = R.drawable.outline_insert_drive_file_24;
    public static final int ICON_TEXT = R.drawable.outline_text_snippet_24;

    public static final int COLOR_FILE_MUSIC = 0xFF9C27B0;
    public static final int COLOR_FILE_VIDEO = 0xFF4CAF50;
    public static final int COLOR_FILE_IMAGE = 0XFFF44336;
    public static final int COLOR_FILE_TEXT = 0xFF00BCD4;
    private static final int COLOR_FILE_PDF = 0xFFF44336;
    public static final int COLOR_FILE_POWERPOINT = 0xFFFF5722;
    public static final int COLOR_FILE_EXCEL = 0xFF009688;
    public static final int COLOR_FILE_WORD = 0xFF3F51B5;
    public static final int COLOR_FILE_ZIP = 0xFFFF9800;
    public static final int COLOR_FILE = 0x9E9E9E;

    public static int getFileIcon(String mimeType) {
        String[] split = mimeType.split("/",2);
        if (split.length != 2)
            return ICON_FILE;
        String type = split[0];
        String subtype = split[1];
        switch (Objects.requireNonNull(type)) {
            case "audio":
                return ICON_FILE_MUSIC;
            case "video":
                return ICON_FILE_VIDEO;
            case "image":
                return ICON_FILE_IMAGE;
            case "text":
                return ICON_FILE_TEXT;
        }
        switch (Objects.requireNonNull(subtype)) {
            case "vnd.ms-powerpoint":
            case "vnd.openxmlformats-officedocument.presentationml.presentation":
            case "pdf":
                return ICON_FILE_POWERPOINT;
            case "vnd.ms-excel":
            case "vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                return ICON_FILE_EXCEL;
            case "msword": // 这个可能火狐那边写错了，所以补了下面一个类型
            case "vnd.ms-word":
            case "vnd.openxmlformats-officedocument.wordprocessingml.document":
                return ICON_FILE_WORD;
            case "x-rar-compressed":
            case "x-tar":
            case "zip":
            case "x-7z-compressed":
            case "java-archive":
            case "vnd.sun.j2me.app-descriptor":
                return ICON_FILE_ZIP;
        }
        return ICON_FILE;
    }

    public static int getFileIconColor(@NonNull Context context,@Nullable String mimeType) {
        if (mimeType==null)
            return ThemeHelper.getThemeColor(context, android.R.attr.textColorSecondary);
        String[] split = mimeType.toLowerCase().split("/",2);
        if (split.length != 2)
            return ThemeHelper.getThemeColor(context, android.R.attr.textColorSecondary);
        String type = split[0];
        String subtype = split[1];
        switch (Objects.requireNonNull(type)) {
            case "audio":
                return COLOR_FILE_MUSIC;
            case "video":
                return COLOR_FILE_VIDEO;
            case "image":
                return COLOR_FILE_IMAGE;
            case "text":
                return COLOR_FILE_TEXT;
        }
        switch (Objects.requireNonNull(subtype)) {
            case "pdf":
                return COLOR_FILE_PDF;
            case "vnd.ms-powerpoint":
            case "vnd.openxmlformats-officedocument.presentationml.presentation":
                return COLOR_FILE_POWERPOINT;
            case "vnd.ms-excel":
            case "vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                return COLOR_FILE_EXCEL;
            case "msword": // 这个可能火狐那边写错了，所以补了下面一个类型
            case "vnd.ms-word":
            case "vnd.openxmlformats-officedocument.wordprocessingml.document":
                return COLOR_FILE_WORD;
            case "x-rar-compressed":
            case "x-tar":
            case "zip":
            case "x-7z-compressed":
                return COLOR_FILE_ZIP;
        }

        return ThemeHelper.getThemeColor(context, android.R.attr.textColorSecondary);
    }
}
