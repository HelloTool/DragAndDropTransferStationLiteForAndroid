package io.github.jesse205.transferstation.lite.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

public class DragDropBridgeActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, DragDropActivity.class);
        intent.putExtra(DragDropActivity.EXTRA_KEEP_IN_SCREEN, true);
        startActivity(intent);
        finishAndRemoveTask();
    }
}
