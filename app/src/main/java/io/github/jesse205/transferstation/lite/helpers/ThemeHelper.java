package io.github.jesse205.transferstation.lite.helpers;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.ContextThemeWrapper;

import io.github.jesse205.transferstation.lite.R;
import io.github.jesse205.transferstation.lite.utils.SystemUtil;

public class ThemeHelper {
    private static final String TAG = "ThemeHelper";

    /**
     * 应用自定义主题
     *
     * @param context 上下文
     */
    public static void applyTheme(Context context) {
        context.setTheme(android.R.style.Theme_DeviceDefault_Settings);
        applyEmuiTheme(context);
        applyMagicTheme(context);
        applyThemeOverlay(context);

    }

    /* public static void applyTheme(Service service) {
        applyThemeOverlay(service);
    } */

    public static void applyThemeOverlay(Context context) {
        context.setTheme(getSystemThemeOverlay());
    }

    public static int getSystemThemeOverlay() {
        if (SystemUtil.isEmuiSystem() || SystemUtil.isMagicSystem()) {
            return R.style.ThemeOverlay_DragAndDropTransferStationLite_Emui;
        } else {
            return R.style.ThemeOverlay_DragAndDropTransferStationLite;
        }
    }

    public static void applyEmuiTheme(Context context) {
        if (SystemUtil.isEmuiSystem()) {
            try {
                Class<?> androidhwextRstyle = Class.forName("androidhwext.R$style");
                Integer themeResId = (Integer) androidhwextRstyle.getField("Theme_Emui").get(null);
                if (themeResId != null)
                    context.setTheme(themeResId);
            } catch (ClassNotFoundException | NoSuchFieldException | NullPointerException |
                     IllegalAccessException ignored) {
            }
        }

    }

    public static void applyMagicTheme(Context context) {
        if (SystemUtil.isMagicSystem()) {
            try {
                Class<?> androidhnextRstyle = Class.forName("androidhnext.R$style");
                Integer themeResId = (Integer) androidhnextRstyle.getField("Theme_Magic").get(null);
                if (themeResId != null)
                    context.setTheme(themeResId);
            } catch (ClassNotFoundException | NoSuchFieldException | NullPointerException |
                     IllegalAccessException ignored) {
            }
        }
    }


    /**
     * 获取主题色
     *
     * @param context 当前上下文
     * @param attr    属性
     * @return 颜色值
     */
    public static int getThemeColor(Context context, int attr) {
        TypedArray array = context.obtainStyledAttributes(new int[]{attr});
        int color = array.getColor(0, 0);
        array.recycle();
        return color;
    }

    /**
     * 获取主题值
     *
     * @param context 当前上下文
     * @param attr    属性
     * @return 颜色值
     */
    public static boolean getThemeBoolean(Context context, int attr) {
        TypedArray array = context.obtainStyledAttributes(new int[]{attr});
        boolean value = array.getBoolean(0, false);
        array.recycle();
        return value;
    }
}
