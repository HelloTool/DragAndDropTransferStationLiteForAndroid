package io.github.jesse205.transferstation.lite.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import io.github.jesse205.transferstation.lite.R;
import io.github.jesse205.transferstation.lite.databinding.ActivityMainBinding;
import io.github.jesse205.transferstation.lite.helpers.ThemeHelper;

public class MainActivity extends Activity {
    ActivityMainBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeHelper.applyTheme(this);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

    }

}
