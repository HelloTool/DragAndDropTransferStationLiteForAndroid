package io.github.jesse205.transferstation.lite.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;

import io.github.jesse205.transferstation.lite.R;
import io.github.jesse205.transferstation.lite.helpers.ThemeHelper;

public class DialogActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        ThemeHelper.applyTheme(this);
        setTheme(R.style.ThemeOverlay_DragAndDropTransferStationLite_DialogActivity_Transparent);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (intent.getIntExtra("title", 0) != 0)
            builder.setTitle(intent.getIntExtra("title", 0));
        else if (intent.getStringExtra("title") != null)
            builder.setTitle(intent.getStringExtra("title"));

        if (intent.getIntExtra("message", 0) != 0)
            builder.setMessage(intent.getIntExtra("message", 0));
        else if (intent.getStringExtra("message") != null)
            builder.setMessage(intent.getStringExtra("message"));

        builder.setPositiveButton(android.R.string.ok, null);
        builder.setOnDismissListener(dialog1 -> finishAndRemoveTask());
        builder.setOnCancelListener(dialogInterface -> finishAndRemoveTask());
        builder.show();

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(false);
            window.setNavigationBarContrastEnforced(false);
            window.setStatusBarContrastEnforced(false);
        } else {
            int systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;

            window.getDecorView().setSystemUiVisibility(systemUiVisibility);
        }
    }
}
