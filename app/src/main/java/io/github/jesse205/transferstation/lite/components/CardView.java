package io.github.jesse205.transferstation.lite.components;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import io.github.jesse205.transferstation.lite.R;


public class CardView extends FrameLayout {

    public CardView(@NonNull Context context) {
        super(context, null);
    }

    public CardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, R.attr.cardViewStyle);
    }

    public CardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.CardView, defStyle, 0);
        float elevation = a.getDimension(
                R.styleable.CardView_cardElevation,
                getContext().getResources().getDimension(R.dimen.cardView_elevation));
        setCardElevation(elevation);

        if (a.hasValue(R.styleable.CardView_cardBackgroundColor)) {
            int bgColor = a.getColor(
                    R.styleable.CardView_cardBackgroundColor,
                    0);
            setCardBackgroundColor(bgColor);
        }
        if (a.hasValue(R.styleable.CardView_cardCornerRadius)) {
            float radius = a.getDimension(
                    R.styleable.CardView_cardCornerRadius,
                    0);
            setRadius(radius);
        }
        a.recycle();
    }


    public void setCardBackgroundColor(int color) {
        ((GradientDrawable) getBackground()).setColor(color);
    }

    public ColorStateList getCardBackgroundColor(int color) {
        return ((GradientDrawable) getBackground()).getColor();
    }

    public void setCardBackgroundAlpha(int alpha) {
        ((GradientDrawable) getBackground()).setAlpha(alpha);
    }

    public void setRadius(float radius) {
        ((GradientDrawable) getBackground()).setCornerRadius(radius);
    }

    public void setCardElevation(float elevation) {
        setElevation(elevation);
    }

}