package io.github.jesse205.transferstation.lite.utils;

import android.os.Build;
import android.util.Log;

import dalvik.system.DexClassLoader;

public class SystemUtil {
    private static final String TAG = "SystemUtil";
    private static final int FLAG_IS_EMUI = 1;
    private static final int FLAG_IS_MAGIC = 1 << 1;
    private static final int FLAG_IS_MIUI = 1 << 2;
    private static int determinedFlags = 0;
    private static int systemTypeFlags = 0;

    /**
     * 判断当前系统是不是EMUI
     *
     * @return 当前系统是不是EMUI
     */
    public static boolean isEmuiSystem() {
        if ((determinedFlags & FLAG_IS_EMUI) != 0) {
            return (systemTypeFlags & FLAG_IS_EMUI) != 0;
        } else {
            determinedFlags |= FLAG_IS_EMUI;
            try {
                Class<?> androidhwextRstyle = Class.forName("androidhwext.R");
                systemTypeFlags |= FLAG_IS_EMUI;
                return true;
            } catch (ClassNotFoundException | NullPointerException ignored) {

            }
        }
        return false;
    }

    /**
     * 判断当前系统是不是MagicOS
     *
     * @return 当前系统是不是MagicOS
     */
    public static boolean isMagicSystem() {
        if ((determinedFlags & FLAG_IS_MAGIC) != 0) {
            return (systemTypeFlags & FLAG_IS_MAGIC) != 0;
        } else {
            determinedFlags |= FLAG_IS_MAGIC;
            try {
                Class<?> androidhwextRstyle = Class.forName("androidhnext.R");
                systemTypeFlags |= FLAG_IS_MAGIC;
                return true;
            } catch (ClassNotFoundException | NullPointerException ignored) {

            }
        }
        return false;
    }

}
