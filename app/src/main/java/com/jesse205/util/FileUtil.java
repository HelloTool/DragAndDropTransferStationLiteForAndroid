package com.jesse205.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

/**
 * 文件工具类
 *
 * @author Jesse205
 */
public class FileUtil {
    public static final String TAG = "FileUtil";

    public static void copyFile(@NonNull FileChannel sourceChannel, @NonNull FileChannel destChannel) throws IOException {
        long size = sourceChannel.size();
        for (long left = size; left > 0; ) {
            left -= sourceChannel.transferTo((size - left), left, destChannel);
        }
    }

    public static void copyFile(@NonNull FileInputStream sourceStream, @NonNull FileOutputStream destStream) throws IOException {
        try (FileChannel sourceChannel = sourceStream.getChannel();
             FileChannel destChannel = destStream.getChannel()) {
            copyFile(sourceChannel, destChannel);
        }
    }

    public static void copyFile(@NonNull InputStream sourceStream, @NonNull OutputStream destStream) throws IOException {
        byte[] b = new byte[1024 * 5];
        int len;
        while ((len = sourceStream.read(b)) != -1) {
            destStream.write(b, 0, len);
        }
        destStream.flush();
    }

    public static void copyFile(@NonNull File sourceFile, @NonNull File destFile) throws IOException {
        try (FileInputStream sourceInputStream = new FileInputStream(sourceFile);
             FileOutputStream destOutputStream = new FileOutputStream(destFile)) {
            copyFile(sourceInputStream, destOutputStream);
        }
    }

    /**
     * 获取扩展名
     *
     * @param name 文件名
     * @return 扩展名
     */
    @Nullable
    public static String getExtensionName(@NonNull String name) {
        String[] splitName = splitFileName(name);
        return splitName[1];
    }

    /**
     * 分割文件名与扩展名
     *
     * @param fileName 文件名
     * @return 分割后的数组
     */
    @NonNull
    public static String[] splitFileName(@NonNull String fileName) {
        int index = fileName.lastIndexOf(".");
        String toPrefix;
        String toSuffix = "";
        if (index == -1) {
            toPrefix = fileName;
        } else {
            toPrefix = fileName.substring(0, index);
            toSuffix = fileName.substring(index + 1);
        }
        return new String[]{toPrefix, toSuffix};
    }


    public static File getNonDuplicateFile(File file) {
        File directory = file.getParentFile();
        assert directory != null;
        if (!directory.exists()) {
            return file;
        }
        String[] splitName = splitFileName(file.getName());
        if (splitName[1] == null) {
            splitName[1] = "";
        }
        File newFile = file;
        int count = 1;
        while (newFile.exists()) {
            count++;
            newFile = new File(directory, splitName[0] + '(' + count + ')' + '.' + splitName[1]);
            if (count == Integer.MAX_VALUE) {
                splitName[0] += '(' + count + ')';
            }
        }
        return newFile;
    }


    public static void deleteDirs(File dir) {
        if (dir == null || !dir.exists() || !dir.isDirectory())
            return;
        File[] fileList = dir.listFiles();
        if (fileList == null)
            return;

        for (File file : fileList) {
            if (file.isFile())
                // noinspection ResultOfMethodCallIgnored
                file.delete();
            else if (file.isDirectory())
                deleteDirs(file); // 递规的方式删除文件夹
        }
        // noinspection ResultOfMethodCallIgnored
        dir.delete();// 删除目录本身
    }

}
