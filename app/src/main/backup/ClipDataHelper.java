package io.github.jesse205.transferstation.lite.helpers;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.webkit.MimeTypeMap;

import com.jesse205.util.FileUtil;

import java.util.List;

public class ClipDataHelper {
    public static final String APP_LABEL = "edde_dadts_lite";
    private static final String TAG = "ClipDataHelper";
    private static final int grantFlag = Intent.FLAG_GRANT_READ_URI_PERMISSION
            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION;

    /**
     * 授予第三方软件文件权限
     * @param context 上下文
     * @param clipData 内容
     */
    public static void grantAdditionalAppsPermissions(Context context, ClipData clipData) {
        ClipDescription description = clipData.getDescription();
        int mimeTypeCount = description.getMimeTypeCount();
        for (int i = 1; i < clipData.getItemCount(); i++) {
            ClipData.Item item = clipData.getItemAt(i);
            Uri uri = item.getUri();
            if (uri != null) {
                MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
                String mimeType = null;
                if (i < mimeTypeCount)
                    mimeType = description.getMimeType(i);
                else {
                    String name = FilesHelper.getName(context, uri);
                    if (name != null) {
                        mimeType = mimeTypeMap.getMimeTypeFromExtension(
                                FileUtil.getExtensionName(name));
                    }
                }
                if (mimeType == null)
                    mimeType = "*/*";


                grantAdditionalAppsPermissions(context, uri, mimeType);
            }
        }
    }

    /**
     * 授予其他应用使用权限。因为华为APP和QQ等应用不会自己申请权限
     *
     * @param uri 文件 uri
     */
    public static void grantAdditionalAppsPermissions(Context context, Uri uri, String type) {

        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setType(type);
        List<ResolveInfo> infoList;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            infoList = packageManager.queryIntentActivities(intent,
                    PackageManager.ResolveInfoFlags.of(PackageManager.MATCH_ALL));
        } else {
            infoList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_ALL);
        }
        try {
            for (int i = 0; i < infoList.size(); i++) {
                context.grantUriPermission(infoList.get(i).activityInfo.packageName, uri, grantFlag);
            }
            context.grantUriPermission("com.huawei.desktop.explorer", uri, grantFlag);
            context.grantUriPermission("com.huawei.desktop.systemui", uri, grantFlag);
            context.grantUriPermission("com.huawei.distributedpasteboard", uri, grantFlag);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

}
